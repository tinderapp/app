'use strict';

app.factory('Auth', function($firebaseAuth, $firebaseObject, $firebaseArray, $state, $http, $q) {
  var ref = firebase.database().ref();
  var auth = $firebaseAuth();

  var Auth = {
          ///////create profile function  ////
    createProfile: function(uid, profile) {
      return ref.child('profiles').child(uid).set(profile);
    },

    /////// Get profile function  ////
    getProfile: function(uid) {
      return $firebaseObject(ref.child('profiles').child(uid));
    },

    /////// get age function  ////
    getAge: function(birthday) {
      return new Date().getFullYear() - new Date(birthday).getFullYear();
    },

    /////// get all profiles function  ////
    getProfiles: function() {
      return $firebaseArray(ref.child('profiles'));
  },

    /////// get profile by age function  ////
    getProfilesByAge: function(age) {
      return $firebaseArray(ref.child('profiles').orderByChild('age').startAt(18).endAt(age));
    },
    ///// set user for online function //////
    setOnline: function(uid){
      var connected = $firebaseObject(ref.child(".info/connected"));
      var online = $firebaseObject(ref.child('profiles').child(uid));

      connected.$watch(function(){
        if(connected.$value === true){
          ref.child('profiles').child(uid).update({
            isOnline: true
          });

          online.$ref().onDisconnect().update({
            isOnline: false
          });
        }
      });
    },
    /////////////////////////////////////////////////////////////////////////////


    ///////// user login function //////////
    login: function() {
      var defer = $q.defer();

      function success(result) {
        var accessToken = result.credential.accessToken;
        var user = Auth.getProfile(result.user.uid).$loaded();

        user.then(function(profile) {
          if (profile.name == undefined) {
            var genderPromise = $http.get('https://graph.facebook.com/me?fields=gender&access_token=' + accessToken);
            var birthdayPromise = $http.get('https://graph.facebook.com/me?fields=birthday&access_token=' + accessToken);
            var locationPromise = $http.get('https://graph.facebook.com/me?fields=location&access_token=' + accessToken);
            var agePromise = $http.get('https://graph.facebook.com/me?fields=age_range&access_token=' + accessToken);
            var bioPromise = $http.get('https://graph.facebook.com/me?fields=about&access_token=' + accessToken);
            var imagesPromise = $http.get('https://graph.facebook.com/me/photos/uploaded?fields=source&access_token=' + accessToken);
            var promises = [genderPromise, birthdayPromise, locationPromise, bioPromise, imagesPromise, agePromise];
            $q.all(promises).then(function(data) {
              var info = result.user.providerData[0];
              var profile = {
                name: info.displayName,
                email: info.email,
                avatar: info.photoURL,
                gender: data[0].data.gender ? data[0].data.gender : "",
                birthday: data[1].data.birthday ? data[1].data.birthday : "",
                age: data[1].data.birthday ? Auth.getAge(data[1].data.birthday) : 20,
                location: data[2].data.location ?  data[2].data.location.name : "",
                bio: data[3].data.about ? data[3].data.about : "",
                images: data[4].data.data
              };
              Auth.createProfile(result.user.uid, profile);
              defer.resolve();
            });
          }
          else defer.resolve(profile);
        });
      }

      function error(error) {
        console.log("error");
        console.log(error);
        defer.reject();
      }

      var fields = ['public_profile, email, user_location, user_birthday, user_photos, user_about_me'];
      if (window.cordova) {
        facebookConnectPlugin.login(fields,
          function(result) {
            provider = firebase.auth.FacebookAuthProvider.credential(result.authResponse.accessToken);
            return auth.$signInWithCredential(provider).then(function(r) {
              result.credential = {accessToken: result.authResponse.accessToken};
              result.user = r;
              success(result);
            }, error);

          }, function(error) {
            console.log("error");
            console.log(error);
            defer.reject();
          });
      }
      else{
        var provider = new firebase.auth.FacebookAuthProvider();
        provider.addScope('public_profile, email, user_location, user_birthday, user_photos, user_about_me');

        auth.$signInWithPopup(provider)
          .then(success, error);
      }

       return defer.promise;
    },

    //////////////////////////////  user logout function ////////////////////////
    logout: function() {
      ref.child('profiles').child(auth.$getAuth().uid).update({isOnline: false});
      return auth.$signOut();
    },

    /////////////////////////////////  required user login or sign in function /////////////////
    requireAuth: function() {
      return auth.$requireSignIn();
    }
  };
    ///////////////////////////// condition for check user login or not /////////////////////////////
  auth.$onAuthStateChanged(function(authData) {
    if(authData) {
      console.log('Logged in!');
    } else {
      $state.go('login');
      console.log('You need to login.');
    }
  });

  return Auth;

});
