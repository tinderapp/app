'use strict';

app.controller('StoryCtrl', function($scope, $timeout) {
  $scope.swipeimages = [
    {
      name: 'In the jungle of India, a group of villagers are sightseeing. Later that night, Shere Khan the tiger ' +
      'and his sidekick Tabaqui the hyena attack the villagers. A young boy named Mowgli runs off in search of his parents,' +
      ' only to get lost. He is eventually taken into a wolf pack by wolf parents, Akela and Raksha.',
      avatar : 'img/one.jpg',
      number : 1
    },
    {
      name: "Mowgli makes friends with Baloo the bear, Bagheera the panther, Chil the vulture," +
      " and Hathi the elephant. One day, Hathi takes Mowgli for a ride on his back in order to give him a tour of the jungle." +
      "Mowgli learns that man poses a danger to the jungle and the animals after seeing part of the jungle " +
      "destroyed by fire.",
      avatar : 'img/two.jpg',
      number : 2
    },
    {
      name: "During a pack meeting, Mowgli and his wolf sister, Little Raksha are chosen to become hunters for the pack, much to" +
      " the dismay of the wolf bullies. Shere Khan confronts the wolf pack and demands they hand over Mowgli. Raksha and Akela refuse, " +
      "saying that Mowgli is their son. Bagheera and Baloo arrive and swear to protect Mowgli from Shere Khan. Shere Khan then " +
      "assures",
      avatar : 'img/three.jpg',
      number : 3
    },
    {
      name: " Mowgli that one day, he will get his revenge, when the pack, Bagheera, or Baloo won't be able to protect him. " +
      "agheera and Baloo attempt to teach Mowgli how to hunt, only resulting in failed attempts from Mowgli. " +
      "Bagheera then makes Mowgli look into her eyes, a trick Shere Khan uses on his victims, believing it will prepare Mowgli, " +
      "should he ever encounter Shere Khan alone. Mowgli learns the lesson and returns to the pack.",
      avatar : 'img/four.jpg',
      number : 4
    },
    {
      name: "One night, the wolf bullies team up with Shere Khan to drive Mowgli out of the pack, all believing that man does not" +
      " belong in a wolf pack. During a hunt, Akela assigns groups for the hunt. Mowgli is teamed with the wolf bullies. They cause" +
      " him to ruin the hunt, making the pack go hungry. Upset, Mowgli decides to run away from the pack.",
      avatar : 'img/five.jpg',
      number : 5
    },
    {
      name: "The next day, Mowgli runs off with some chimpanzees to Monkey Town, for they claim a 'surprise party'." +
      " Unbeknown to Mowgli, the chimps have set up a plan with Shere Khan. They trap him in a wooden house, thus making " +
      "him vulnerable for Shere Khan. Chil flies off to tell Raksha. Raksha races to Monkey Town to rescue Mowgli.",
      avatar : 'img/six.jpg',
      number : 6
    },
    {
      name: "The chimps assure Shere Khan that Mowgli is trapped. Shere Khan proceeds to Monkey Town to kill Mowgli. At Monkey Town," +
      " Shere Khan is confronted by Raksha. Shere Khan attacks Raksha and kills her. Bagheera and Baloo rescue Mowgli and take" +
      " him away from Monkey Town.",
      avatar : 'img/seven.jpg',
      number : 7
    },
    {
      name: "The next day, Hathi informs Mowgli that an incident has occurred involving Raksha. Hathi takes Mowgli back to the pack," +
      " where Raksha's lifeless corpse is laid out. Akela, Little Raksha and Mowgli mourn Raksha's death. Blaming himself, Mowgli " +
      "tries to run away from the jungle. Little Raksha runs off to try and stop Mowgli.",
      avatar : 'img/eight.jpg',
      number : 8
    },
    {
      name: "Mowgli stumbles upon a village, seeing his own kind. Mowgli then hears Little Raksha's cries for help and returns." +
      " to the jungle to aid her. He frees her from a bear trap. Little Raksha then reminds Mowgli that he took the Hunter's Oath " +
      "and shouldn't run away. Mowgli realises that he must face Shere Khan. He decides not to face Shere Khan as a wolf, but as a man.",
      avatar : 'img/nine.jpg',
      number : 9
    },
    {
      name: "That night, Shere Khan falls for Mowgli's trap, a large circle made from vines. With Shere Khan in the circle," +
      " Mowgli sets the vines on fire using a match he found at the wooden house back at Monkey Town, trapping Shere Khan by" +
      " surrounding him with flames. Mowgli then banishes Shere Khan from the jungle as Bagheera, Baloo, Little Raksha and the " +
      "wolf bullies look on. Shere Khan swears never to return to the jungle, so Mowgli allows him to leave. Mowgli is praised by",
      avatar : 'img/ten.jpg',
      number : 10
    },
    {
      name: "Everyone, including the wolf bullies, who admit that they were wrong about him. The role of leader of the pack is offered" +
      " to Mowgli, but he turns it down and gives it to Little Raksha. The next day, Bagheera and Baloo give Mowgli a book" +
      " featuring jungle animals. He thanks them and then runs off.",
      avatar : 'img/eleven.jpg',
      number : 11
    }
  ];
  $scope.resetSwipeImages = angular.copy($scope.swipeimages);

  $scope.show = true;
  $scope.cardDestroyed = function(index) {
    var card = $scope.swipeimages[index];
    $scope.swipeimages.splice(index, 1);
  };
  $scope.resetImages = function(){
    $scope.show = false;
    $timeout(function(){
      $scope.swipeimages = angular.copy($scope.resetSwipeImages);
      $scope.show = true;
    }, 100);
  }
});
